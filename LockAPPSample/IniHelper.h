#pragma once

#define MAX_DETECT_VALUE_LENGTH        512

class CIniHelper
{
private:
	CIniHelper(void); //私有构造函数

public:
	virtual ~CIniHelper(void);

	static CIniHelper* Instance() /* 采用单例模式设计 */
	{
		if (m_pInstance == NULL)  
			m_pInstance = new CIniHelper();  
		return m_pInstance;
	}

public:
	CString SetIniFileName(CString fileName);

public:
	VOID WriteString(CString& sec, LPCTSTR key, CString& value)
	{
		::WritePrivateProfileString(sec, key, value, m_daoPath);
	}

	VOID WriteString(LPCTSTR sec, LPCTSTR key, CString& value)
	{
		::WritePrivateProfileString(sec, key, value, m_daoPath);
	}

	VOID WriteInt(CString& sec, LPCTSTR key, INT32 value)
	{
		TCHAR buff[MAX_DETECT_VALUE_LENGTH] = { 0 };

		_stprintf_s(buff, MAX_DETECT_VALUE_LENGTH, _T("%d"), value);
		::WritePrivateProfileString(sec, key, buff, m_daoPath);
	}

	INT32 GetInt(CString& sec, LPCTSTR key, LPCTSTR defaultValue = _T(""))
	{
		INT32 value;
		TCHAR buff[MAX_DETECT_VALUE_LENGTH] = { 0 };

		::GetPrivateProfileString(sec, key, defaultValue, buff, 
			MAX_DETECT_VALUE_LENGTH, m_daoPath);
		_stscanf(buff, _T("%d"), &value);

		return value;
	}

	CString GetString(CString& sec, LPCTSTR key)
	{
		TCHAR buff[MAX_DETECT_VALUE_LENGTH] = { 0 };

		::GetPrivateProfileString(sec, key, _T(""), buff, MAX_DETECT_VALUE_LENGTH, m_daoPath);

		return buff;
	}

	CString GetString(LPCTSTR sec, LPCTSTR key)
	{
		TCHAR buff[MAX_DETECT_VALUE_LENGTH] = { 0 };

		::GetPrivateProfileString(sec, key, _T(""), buff, MAX_DETECT_VALUE_LENGTH, m_daoPath);

		return buff;
	}

	CString GetString(CString& path, CString& sec, LPCTSTR key)
	{
		TCHAR buff[MAX_DETECT_VALUE_LENGTH] = { 0 };

		::GetPrivateProfileString(sec, key, _T(""), buff, MAX_DETECT_VALUE_LENGTH, path);

		return buff;
	}

public:
	//VOID LoadDetectModel(CStdPtrArray& list);

private:
	class CGarbo //它的唯一工作就是在析构函数中删除CSingleton的实例  
	{  
	public:  
		~CGarbo()  
		{  
			if (CIniHelper::m_pInstance)  
				delete CIniHelper::m_pInstance;  
		}  
	}; 

private:
	CString m_daoPath;

	static CIniHelper* m_pInstance;
	static CGarbo Garbo; // 定义一个静态成员，在程序结束时，系统会调用它的析构函数  
};
