#include "StdAfx.h"
#include "AutoRun.h"

CAutoRun::CAutoRun(void)
{
}

CAutoRun::~CAutoRun(void)
{
}

void CAutoRun::install()
{
	//添加以下代码
	HKEY   hKey; 
	TCHAR pFileName[MAX_PATH] = {0}; 
	//得到程序自身的全路径 
	DWORD dwRet = GetModuleFileName(NULL, pFileName, MAX_PATH); 
	//找到系统的启动项 
	LPCTSTR lpRun = _T("Software\\Microsoft\\Windows\\CurrentVersion\\Run"); 
	//打开启动项Key 
	long lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, lpRun, 0, KEY_WRITE, &hKey); 
	if(lRet== ERROR_SUCCESS)
	{
		//添加注册
		RegSetValueEx(hKey, _T("LockAPP"), 0,REG_SZ,(const BYTE*)(LPCSTR)pFileName, MAX_PATH);
		RegCloseKey(hKey); 
	}
}

void CAutoRun::unInstall()
{
	//添加以下代码
	HKEY   hKey; 
	TCHAR pFileName[MAX_PATH] = {0}; 
	//得到程序自身的全路径 
	DWORD dwRet = GetModuleFileName(NULL, pFileName, MAX_PATH); 
	//找到系统的启动项 
	LPCTSTR lpRun = _T("Software\\Microsoft\\Windows\\CurrentVersion\\Run"); 
	//打开启动项Key 
	long lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, lpRun, 0, KEY_WRITE, &hKey); 
	if(lRet== ERROR_SUCCESS)
	{
		//删除注册
		RegDeleteValue(hKey,_T("LockAPP"));
		RegCloseKey(hKey); 
	}
}
