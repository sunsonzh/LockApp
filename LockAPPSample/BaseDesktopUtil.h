#pragma once

class CBaseDesktopUtil
{
public:
	CBaseDesktopUtil(void);
	~CBaseDesktopUtil(void);

public:
	//WPSTYLE_CENTER ���� 0
	//WPSTYLE_TILE ƽ�� 1
	//WPSTYLE_STRETCH ���� 2 
	//WPSTYLE_MAX 3
	virtual BOOL SetWallpaper(LPTSTR lpPicFile, DWORD dwStyle) = 0;

	virtual void onWTSConsoleConnect(LPTSTR lpPicFile) {};
	virtual void onWTSConsoleDisConnect() {};

	virtual void onWTSSessionLock(LPTSTR lpPicFile){};
	virtual void onWTSSessionUnlock(){};

	virtual void onWTSSessionLogon(LPTSTR lpPicFile){};
	virtual void onWTSSessionLogoff(){};

	virtual void onStart() {};

	virtual void onExit() {};

	TCHAR* getWallPaper();
};
