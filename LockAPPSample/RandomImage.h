#pragma once

class CRandomImage
{
private:
	CRandomImage(void);
public:
	~CRandomImage(void);

	void init(LPTSTR path);
	const CString getRandomImage();

	static CRandomImage* Instance() /* 采用单例模式设计 */
	{
		if (m_pInstance == NULL)  
			m_pInstance = new CRandomImage();  
		return m_pInstance;
	}

private:
	void search(LPTSTR path);

	class CGarbo //它的唯一工作就是在析构函数中删除CSingleton的实例  
	{  
	public:  
		~CGarbo()  
		{  
			if (CRandomImage::m_pInstance)  
				delete CRandomImage::m_pInstance;  
		}  
	}; 

private:
	LPTSTR m_lptPath;
	INT32  m_iRandomIndex;
	CStringArray m_strPathArray;

	static CRandomImage* m_pInstance;
	static CGarbo Garbo; // 定义一个静态成员，在程序结束时，系统会调用它的析构函数  
};
