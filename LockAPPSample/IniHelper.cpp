#include "StdAfx.h"
#include "IniHelper.h"
#include "AppUtil.h"

CIniHelper* CIniHelper::m_pInstance = NULL;

CIniHelper::CIniHelper(void)
{
	m_daoPath = CAppUtil::getApplicationDirectory();
	m_daoPath.Append(_T("Config.ini"));
}

CIniHelper::~CIniHelper(void)
{
}

CString CIniHelper::SetIniFileName(CString fileName)
{
	CString oldFileName;

	oldFileName = m_daoPath;
	m_daoPath   = fileName;

	return oldFileName;
}