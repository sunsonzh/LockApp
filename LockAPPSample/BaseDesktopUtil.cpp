#include "StdAfx.h"
#include "BaseDesktopUtil.h"

CBaseDesktopUtil::CBaseDesktopUtil(void)
{
}

CBaseDesktopUtil::~CBaseDesktopUtil(void)
{
}

TCHAR* CBaseDesktopUtil::getWallPaper()
{
	BOOL result; 
	static TCHAR oldWallPaper[MAX_PATH] = { 0 }; 

	memset(oldWallPaper, 0, sizeof(oldWallPaper));

	result = SystemParametersInfo(SPI_GETDESKWALLPAPER, sizeof(oldWallPaper), &oldWallPaper, 0);

	return oldWallPaper;
}