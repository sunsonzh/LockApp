#include "StdAfx.h"
#include "XPDesktopUtil.h"
#include<shlobj.h>
#include "IniHelper.h"
#include "AppUtil.h"
#include "shlwapi.h"

CXPDesktopUtil::CXPDesktopUtil(void)
{
}

CXPDesktopUtil::~CXPDesktopUtil(void)
{
}

BOOL CXPDesktopUtil::SetWallpaper(LPTSTR lpPicFile, DWORD dwStyle)
{
	HRESULT hr; 
	IActiveDesktop* pIAD;   //创建接口的实例  
	CoInitialize(NULL);  
	hr = CoCreateInstance(CLSID_ActiveDesktop,NULL,CLSCTX_INPROC_SERVER,IID_IActiveDesktop,(void**)&pIAD); 
	if(!SUCCEEDED(hr))
	{
		return FALSE; 
	}

	//将文件名改为宽字符串,这是IActiveDesktop::SetWallpaper的要求 
	WCHAR wszWallpaper[MAX_PATH]; 
	MultiByteToWideChar(CP_ACP,0,lpPicFile,-1,wszWallpaper,MAX_PATH); 
	//设置墙纸 

	hr = pIAD-> SetWallpaper(wszWallpaper, 0); 
	if(!SUCCEEDED(hr))   
	{
		return TRUE; 
	}

	//设置墙纸的样式 
	WALLPAPEROPT wpo; 
	wpo.dwSize = sizeof(wpo); 
	wpo.dwStyle = dwStyle; 
	hr = pIAD->SetWallpaperOptions(&wpo,0); 
	if(!SUCCEEDED(hr))   
	{
		return FALSE; 
	}

	//应用墙纸的设置 
	hr = pIAD-> ApplyChanges(AD_APPLY_ALL); 
	if(!SUCCEEDED(hr)) 
	{
		return FALSE;
	}

	//释放接口的实例 
	pIAD-> Release(); 
	CoUninitialize(); 

	return   TRUE;
}

void CXPDesktopUtil::onWTSSessionLock(LPTSTR lpPicFile)
{
	TCHAR *pWallPaper = getWallPaper();

	CString str = backupWallPaper(pWallPaper);

	CIniHelper::Instance()->WriteString("XP", "WallPaper", str);

	util::Log::getInstance()->logging(LOG_INFO, "Saving Windows XP desktop. Desktop file: %s", str.GetBuffer());

	SetWallpaper(lpPicFile, 2);
}

void CXPDesktopUtil::onWTSSessionUnlock()
{
	CString wallPaper = CIniHelper::Instance()->GetString("XP", "WallPaper");

	util::Log::getInstance()->logging(LOG_INFO, "Restore Windows XP desktop. Desktop file: %s", wallPaper.GetBuffer());

	SetWallpaper(wallPaper.GetBuffer(), 2);
}

CString CXPDesktopUtil::backupWallPaper(LPTSTR lpPicFile)
{
	TCHAR szPath[MAX_PATH] = { 0 };

	CString strBackup = CAppUtil::getApplicationDirectory() + "backup";

	if (FALSE == PathFileExists(strBackup.GetBuffer())) {
		if(FALSE == CreateDirectory(strBackup.GetBuffer(), NULL)){
			return _T("");
		}
	}

	strBackup += _T("\\backgroundDefault.jpg");

	::CopyFile(lpPicFile, strBackup.GetBuffer(), FALSE);

	return strBackup;
}

void CXPDesktopUtil::onStart()
{
	//关闭快速用户切换
	HKEY hKey; 

	LPCTSTR lpRun = _T("SOFTWARE\\Microsoft\\\Windows NT\\CurrentVersion\\Winlogon");
	long lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, lpRun, 0, KEY_WRITE, &hKey); 
	if(lRet== ERROR_SUCCESS)
	{
		DWORD dwValue = 0;
		lRet = RegSetValueEx(hKey, _T("AllowMultipleTSSessions"), 0, REG_DWORD, (LPBYTE)&dwValue, sizeof(dwValue));
		if (ERROR_SUCCESS == lRet)
		{
		}
		else
		{
			DWORD dwError = GetLastError();
		}

		RegCloseKey(hKey); 
	}
}

