#pragma once
#include <Windows.h>
#include "BaseDesktopUtil.h"

class CXPDesktopUtil : public CBaseDesktopUtil
{
public:
	CXPDesktopUtil(void);
	~CXPDesktopUtil(void);

	//WPSTYLE_CENTER ���� 0
	//WPSTYLE_TILE ƽ�� 1
	//WPSTYLE_STRETCH ���� 2 
	//WPSTYLE_MAX 3
	BOOL SetWallpaper(LPTSTR lpPicFile, DWORD dwStyle);

	void onWTSSessionLock(LPTSTR lpPicFile);
	void onWTSSessionUnlock();

	CString backupWallPaper(LPTSTR lpPicFile);

	void onStart();
};
