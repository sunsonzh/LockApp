#include "StdAfx.h"
#include "Win7DesktopUtil.h"
#include "shlwapi.h"
#include "RandomImage.h"

CWin7DesktopUtil::CWin7DesktopUtil(void)
	:myWow64DisableWow64FsRedirection(NULL),
	 myMyWow64RevertWow64FsRedirection(NULL)
{
	HMODULE hHandle = LoadLibrary(_T("Kernel32.dll"));

	if (hHandle != NULL)
	{
		//动态加载，防止XP运行异常
		myWow64DisableWow64FsRedirection = (MyWow64DisableWow64FsRedirection)GetProcAddress(hHandle
			, _T("Wow64DisableWow64FsRedirection"));
		myMyWow64RevertWow64FsRedirection = (MyWow64RevertWow64FsRedirection)GetProcAddress(hHandle
			, _T("Wow64RevertWow64FsRedirection"));
	}
}

CWin7DesktopUtil::~CWin7DesktopUtil(void)
{
}

BOOL CWin7DesktopUtil::SetWallpaper(LPTSTR lpPicFile, DWORD dwStyle)
{
	PVOID OldValue = NULL;

	if (myWow64DisableWow64FsRedirection != NULL)
	{
		(*myWow64DisableWow64FsRedirection)(&OldValue); 
	}

	util::Log::getInstance()->logging(LOG_INFO, _T("Set OEMBackground regkey value to 1."));
	if (TRUE == setOEMBackground(1)) 
	{
		copyFile(lpPicFile);
	}

	if (myMyWow64RevertWow64FsRedirection != NULL)
	{
		(*myMyWow64RevertWow64FsRedirection)(OldValue);
	}

	return TRUE;
}

void CWin7DesktopUtil::onWTSSessionLock(LPTSTR lpPicFile)
{
	SetWallpaper(lpPicFile, 2);
}

void CWin7DesktopUtil::onWTSSessionUnlock()
{
	//do nothing
}

void CWin7DesktopUtil::copyFile(LPTSTR lpPicFile)
{
	TCHAR szPath[MAX_PATH] = { 0 };

	_tcscpy(szPath, _T("C:\\Windows\\System32\\oobe"));

	if (FALSE == PathFileExists(szPath)) {
		if(FALSE == CreateDirectory(szPath, NULL)){
			return;
		}
	}

	_tcscat(szPath, _T("\\info"));
	if (FALSE == PathFileExists(szPath)) {
		if(FALSE == CreateDirectory(szPath, NULL)){
			return;
		}
	}
	
	_tcscat(szPath, _T("\\Backgrounds"));
	if (FALSE == PathFileExists(szPath)) {
		if(FALSE == CreateDirectory(szPath, NULL)){
			return;
		}
	}

	_tcscat(szPath, _T("\\backgroundDefault.jpg"));
	::CopyFile(lpPicFile, szPath, FALSE);
}

void CWin7DesktopUtil::onStart()
{
	//win7收到锁屏后的消息再去设置已经晚了，所以程序启动时先设置一下
	CString image = CRandomImage::Instance()->getRandomImage();

	util::Log::getInstance()->logging(LOG_INFO, "onStart...random image: %s", image.GetBuffer());

	onWTSSessionLock(image.GetBuffer());
}

void CWin7DesktopUtil::onExit()
{
	PVOID OldValue = NULL;

	if (myWow64DisableWow64FsRedirection != NULL)
	{
		(*myWow64DisableWow64FsRedirection)(&OldValue); 
	}

	util::Log::getInstance()->logging(LOG_INFO, "onExit Restore OEMBackground regkey value to 0.");
	setOEMBackground(0);

	if (myMyWow64RevertWow64FsRedirection != NULL)
	{
		(*myMyWow64RevertWow64FsRedirection)(OldValue);
	}
}

BOOL CWin7DesktopUtil::setOEMBackground(DWORD dwValue)
{
	HKEY hKey; 

	LPCTSTR lpRun = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Authentication\\LogonUI\\Background");
	long lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, lpRun, 0, KEY_WRITE|KEY_WOW64_64KEY, &hKey); 
	if(lRet== ERROR_SUCCESS)
	{
		lRet = RegSetValueEx(hKey, _T("OEMBackground"), 0, REG_DWORD, (LPBYTE)&dwValue, sizeof(dwValue));
		if (ERROR_SUCCESS == lRet)
		{
			RegCloseKey(hKey); 
			return TRUE;
		}
		else
		{
			DWORD dwError = GetLastError();
		}

		RegCloseKey(hKey); 
	}

	return FALSE;
}
