// LockAPPSampleDlg.h : 头文件
//

#pragma once
#include "TrayIcon.h"
#include "RandomImage.h"
#include "BaseDesktopUtil.h"

// CLockAPPSampleDlg 对话框
class CLockAPPSampleDlg : public CDialog
{
// 构造
public:
	CLockAPPSampleDlg(CWnd* pParent = NULL);	// 标准构造函数
	~CLockAPPSampleDlg();

// 对话框数据
	enum { IDD = IDD_LOCKAPPSAMPLE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

private:
	CTrayIcon m_TrayIcon;
	CBaseDesktopUtil *m_pDesktopUtil;
private:
	afx_msg void OnDemoAbout();
	afx_msg void OnDemoExit();
	LRESULT OnTrayNotification(WPARAM wParam,LPARAM lParam);
};
