#pragma once

enum os_ver {
	WIN_95,
	WIN_98,
	WIN_ME,
	WIN_NT_3_5,
	WIN_2000,
	WIN_XP,
	WIN_2003,
	VISTA,
	WIN_7,
	WIN_8
};

class CAppUtil
{
public:
	CAppUtil(void);
	~CAppUtil(void);

public:
	static CString getApplicationDirectory();

	static os_ver getOSVersion();
};
