// LockAPPSampleDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "LockAPPSample.h"
#include "LockAPPSampleDlg.h"

#include "Wtsapi32.h"
#include "AppUtil.h"
#include "Win7DesktopUtil.h"
#include "XPDesktopUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_ICON_NOTIFY  WM_USER+10
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CLockAPPSampleDlg 对话框




CLockAPPSampleDlg::CLockAPPSampleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLockAPPSampleDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	enum os_ver ver = CAppUtil::getOSVersion();
	if (ver == WIN_XP|| ver == WIN_2003) {
		util::Log::getInstance()->logging(LOG_INFO, "Create CXPDesktopUtil object.");
		m_pDesktopUtil = new CXPDesktopUtil();
	}else{
		util::Log::getInstance()->logging(LOG_INFO, "Create CWin7DesktopUtil object.");
		m_pDesktopUtil = new CWin7DesktopUtil();
	}
}

CLockAPPSampleDlg::~CLockAPPSampleDlg() {
	
}

void CLockAPPSampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CLockAPPSampleDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_DEMO_ABOUT, &CLockAPPSampleDlg::OnDemoAbout)
	ON_COMMAND(ID_DEMO_EXIT, &CLockAPPSampleDlg::OnDemoExit)
	ON_MESSAGE(WM_ICON_NOTIFY, OnTrayNotification)
END_MESSAGE_MAP()


// CLockAPPSampleDlg 消息处理程序

BOOL CLockAPPSampleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	m_TrayIcon.Create(this, WM_ICON_NOTIFY, _T("锁屏小程序"), m_hIcon, IDR_MENU1); //构造
	//ShowWindow(SW_HIDE); //隐藏窗口
	SetWindowPos(&CWnd::wndNoTopMost, 0, 0, 0, 0, SWP_HIDEWINDOW); 
	this->ModifyStyleEx(WS_EX_APPWINDOW, WS_EX_TOOLWINDOW);

	WTSRegisterSessionNotification(m_hWnd, NOTIFY_FOR_ALL_SESSIONS);

	CString path = CAppUtil::getApplicationDirectory() + "images";
	CRandomImage::Instance()->init(path.GetBuffer());
	//一点初始化工作
	m_pDesktopUtil->onStart();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CLockAPPSampleDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CLockAPPSampleDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CLockAPPSampleDlg::OnDemoAbout()
{
	// TODO: Add your command handler code here
	CAboutDlg dlg;

	dlg.DoModal();
}

void CLockAPPSampleDlg::OnDemoExit()
{
	// TODO: Add your command handler code here
	m_pDesktopUtil->onExit();

	m_TrayIcon.RemoveIcon();
	
	OnCancel();
}

LRESULT CLockAPPSampleDlg::OnTrayNotification(WPARAM wParam,LPARAM lParam)
{
	return m_TrayIcon.OnTrayNotification(wParam,lParam);
}

LRESULT CLockAPPSampleDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	switch(message)
	{
	case WM_WTSSESSION_CHANGE:
		{
			//MessageBox("WM_WTSSESSION_CHANGE", "Esmile", MB_OK);

			switch(wParam)
			{
			case WTS_CONSOLE_CONNECT:
				util::Log::getInstance()->logging(LOG_INFO, "WTS_CONSOLE_CONNECT...");
				break;
			case WTS_CONSOLE_DISCONNECT:
				util::Log::getInstance()->logging(LOG_INFO, "WTS_CONSOLE_DISCONNECT...");
				break;
			case WTS_SESSION_LOCK:
				{
					CString image = CRandomImage::Instance()->getRandomImage();
					util::Log::getInstance()->logging(LOG_INFO, "WTS_SESSION_LOCK...random image: %s", image.GetBuffer());
					m_pDesktopUtil->onWTSSessionLock(image.GetBuffer());
				}
				
				break;
			case WTS_SESSION_UNLOCK:
				{
					util::Log::getInstance()->logging(LOG_INFO, "WTS_SESSION_UNLOCK...");
					m_pDesktopUtil->onWTSSessionUnlock();
				}
				break;
			case WTS_SESSION_LOGOFF:
				util::Log::getInstance()->logging(LOG_INFO, "WTS_SESSION_LOGOFF...");
				break;
			case WTS_SESSION_LOGON:
				util::Log::getInstance()->logging(LOG_INFO, "WTS_SESSION_LOGON...");
				break;
			default:
				break;
			}

		}
		break;
	case WM_DESTROY:
		WTSUnRegisterSessionNotification(m_hWnd);
		break;


	default:
		break;
	}

	return CDialog::WindowProc(message, wParam, lParam);
}
