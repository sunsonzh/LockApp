#include "StdAfx.h"
#include "AppUtil.h"

CAppUtil::CAppUtil(void)
{
}

CAppUtil::~CAppUtil(void)
{
}

CString CAppUtil::getApplicationDirectory()
{
	TCHAR szPath[MAX_PATH] = { 0 };
	::GetModuleFileName(NULL, szPath, sizeof(szPath));

	CString strPath = (CString)szPath;
	int position = strPath.ReverseFind('\\');
	strPath = strPath.Left(position + 1); 

	return strPath;
}

/************************************************************************/
/* https://msdn.microsoft.com/en-us/library/windows/desktop/ms724832(v=vs.85).aspx
* Operating system	Version number
* Windows 10                           	10.0*
* Windows Server 2016 Technical Preview	10.0*
* Windows 8.1                         	6.3*
* Windows Server 2012 R2                6.3*
* Windows 8	                            6.2
* Windows Server 2012	                6.2
* Windows 7	                            6.1
* Windows Server 2008 R2	            6.1
* Windows Server 2008	                6.0
* Windows Vista	                        6.0
* Windows Server 2003 R2	            5.2
* Windows Server 2003	                5.2
* Windows XP 64-Bit Edition	            5.2
* Windows XP	                        5.1
* Windows 2000	                        5.0
*/
/************************************************************************/

os_ver CAppUtil::getOSVersion()
{
	DWORD  dwMajorVersion;
	DWORD   dwMinorVersion;
	DWORD  dwBuildNumber;
	DWORD  dwPlatformId;
	OSVERSIONINFO osvi;//定义OSVERSIONINFO数据结构对象
	memset(&osvi, 0, sizeof(OSVERSIONINFO));//开空间 
	osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);//定义大小 
	GetVersionEx (&osvi);//获得版本信息 
	dwMajorVersion=osvi.dwMajorVersion;//主版本号
	dwMinorVersion=osvi.dwMinorVersion;//副版本
	dwBuildNumber=osvi.dwBuildNumber;//创建号
	dwPlatformId=osvi.dwPlatformId;//ID号

	util::Log::getInstance()->logging(LOG_INFO, _T("os version: %d.%d"), dwMajorVersion, dwMinorVersion);

	if (dwMajorVersion == 4 && dwMinorVersion == 0) {
		return WIN_95;
	}else if (dwMajorVersion == 4 && dwMinorVersion == 1) {
		return WIN_98;
	}else if(dwMajorVersion == 4 && dwMinorVersion == 9) {
		return WIN_ME;
	}else if(dwMajorVersion == 3 && dwMinorVersion == 5) {
		return WIN_NT_3_5;
	}else if(dwMajorVersion == 5 && dwMinorVersion == 0) {
		return WIN_2000;
	}else if(dwMajorVersion == 5 && dwMinorVersion == 1) {
		return WIN_XP;
	}else if(dwMajorVersion == 5 && dwMinorVersion == 2) {
		return WIN_2003;
	}else if(dwMajorVersion == 6 && dwMinorVersion == 0) {
		return VISTA;
	}else if(dwMajorVersion == 6 && dwMinorVersion == 1) {
		return WIN_7;
	}else if(dwMajorVersion == 6 && dwMinorVersion == 2) {
		return WIN_8;
	}else {
		return WIN_7; //假定都是win7
	}
}
