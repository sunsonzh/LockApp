#pragma once

#include "BaseDesktopUtil.h"

typedef BOOL (_stdcall *MyWow64DisableWow64FsRedirection)(PVOID *OldValue);
typedef BOOL (_stdcall *MyWow64RevertWow64FsRedirection)(PVOID OldValue);

class CWin7DesktopUtil : public CBaseDesktopUtil
{
public:
	CWin7DesktopUtil(void);
	~CWin7DesktopUtil(void);

public:
	BOOL SetWallpaper(LPTSTR lpPicFile, DWORD dwStyle);

	void onWTSSessionLock(LPTSTR lpPicFile);
	void onWTSSessionUnlock();

	void onStart();
	void onExit();

private:
	void copyFile(LPTSTR lpPicFile);
	BOOL setOEMBackground(DWORD dwValue);

private:
	MyWow64DisableWow64FsRedirection myWow64DisableWow64FsRedirection;
	MyWow64RevertWow64FsRedirection myMyWow64RevertWow64FsRedirection;
};
